# firefighter_prototype

![](images/proto.JPG)

## Context

This repository aims to create a firefighter robot prototype.

Thanks to this protoype, a user can extinguish a fire remotely. This task is divided into 4 features:
1. Move the prototype near the fire
1. Aim the fire source
1. Throw water
1. To do this remotely: create an app in manual mode or create an autonomous mode 

## Visuals

A poster and a video of the final version of the prototype are available in the images folder.

![](images/Poster.png)

The Youtube link for the video is: https://www.youtube.com/watch?v=xZPiqBb18iE

Include screenshots or video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation

1. ROS noetic on Ubuntu MATE 20.04 with:
	1. rosserial
1. Arduino IDE = arduino-1.8.16
	1. roslib : installed with rosserial
	1. ESP32 board
	1. analogWrite

## Usage

1. After git clone this repository, open 4 terminals with firefighter_prototype as working directory
1. Run catkin_make in this terminal to create the devel and build folders from the src folder
1. Run source devel/setup.bash
1. On 1, execute roscore
1. On 1, execute rosrun rosserial_arduino serial_node.py _port:=/dev/ttyUSB0 (no need to change the baudrate, the firefighter robot is to 57600 as the default value)
1. On 1, execute rosrun robot_model firefighter_prototype.py
1. On 1, execute rosrun robot_model turret.py

How to use fire detection : Launch fire_detector.py

## Roadmap

Next features:
1. Autonomous mode

## Authors and acknowledgment

1. AMIRAT Hani
1. ENGEL Etienne
1. FLEITH William
1. FOUADH Nassim

## License

The MIT License

## Source

From qrbx youtube video https://www.youtube.com/watch?v=TUHFWsDPYJ4:
	https://github.com/ROBOTIS-GIT/DynamixelSDK
	https://github.com/aakieu/ax12_control/blob/main/Ax12.py
